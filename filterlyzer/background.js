/* eslint-env webextensions, browser */
/* eslint no-console: 0 */

"use strict";

let extensionTabId;
let requestedUrl;
let filterInput = "";

function isSafeRespHeader(header)
{
  let headerName = header.name.toLowerCase();
  return headerName !== "content-security-policy" &&
    headerName !== "referrer-policy" &&
    headerName !== "x-frame-options" &&
    headerName !== "x-permitted-cross-domain-policies" &&
    headerName !== "x-xss-protection";
}

function analyzeThis(tabId)
{
  chrome.tabs.get(tabId, tabInfo =>
  {
    requestedUrl = tabInfo.url;

    if (~requestedUrl.indexOf("http"))
    {
      if (extensionTabId)
      {
        chrome.tabs.remove(extensionTabId);
        extensionTabId = void 0;
      }

      filterInput = "";
      chrome.tabs.create({url: "/filterlyzer/main.html"},
                         tab =>
                         {
                           extensionTabId = tab.id;
                         });
    }
  });
}

/**
 * Adds a listener to the onBeforeRequest event
 */
chrome.webRequest.onBeforeRequest.addListener(
  details =>
  {
    if (~`${details.url}`.indexOf(chrome.extension.getURL("")))
      return;

    if (details.tabId === extensionTabId)
    {
      let blocked = false;

      if (filterInput && ~filterInput.indexOf("http"))
      {
        if (new RegExp(filterInput.replace(/\//g, "\\/")
                       .replace(/\./g, "\\.")
                       .replace(/\*/, ".*")).test(details.url))
          blocked = true;
      }

      chrome.tabs.sendMessage(extensionTabId,
                              {action: "beforeRequest", details, blocked});

      if (blocked)
        return {cancel: true};
    }
  },
  {urls: ["<all_urls>"]},
  ["blocking"]
);

/**
 * Adds a listener to the onBeforeSendHeaders event
 */
chrome.webRequest.onBeforeSendHeaders.addListener(
  details =>
  {
    if (details.tabId === extensionTabId)
    {
      chrome.tabs.sendMessage(extensionTabId,
                              {action: "beforeSendHeaders", details});
    }
  },
  {urls: ["<all_urls>"]},
  ["requestHeaders", "blocking"/* , "extraHeaders"*/]
);

/**
 * Adds a listener to the onHeadersReceived event
 */
chrome.webRequest.onHeadersReceived.addListener(
  details =>
  {
    if (details.tabId === extensionTabId)
    {
      chrome.tabs.sendMessage(extensionTabId,
                              {action: "onHeadersReceived", details});
      let retHeaders = details.responseHeaders.filter(isSafeRespHeader);

      return {responseHeaders: retHeaders.concat([
        {name: "Access-Control-Allow-Origin", value: "*"}
      ])};
    }
  },
  {urls: ["<all_urls>"]},
  ["responseHeaders", "blocking"/* , "extraHeaders"*/]
);

// Message Receivers
chrome.runtime.onMessage.addListener(
  (request, sender, sendResponse) =>
  {
    switch (request.action)
    {
      case "debug":
        console.log("Debug msg:", request.msg);
        sendResponse({});
        break;

      case "analyzeThis":
        analyzeThis(request.tabId);
        sendResponse({});
        break;

      case "url":
        sendResponse({});
        chrome.tabs.sendMessage(extensionTabId,
                                {action: "url", url: requestedUrl});
        break;

      case "filterInputChanged":
        filterInput = request.filterInput;
        sendResponse({});
        break;

      default:
        console.log("Filterlyzer: Got an unexpected message:", request.action);
        sendResponse({});
    }
  });
