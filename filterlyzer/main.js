/* eslint-env browser, webextensions */
/* eslint no-console: 0 */

"use strict";

/**
 * Stores divs for tabs and their option divs
 */
let optionDivs;
let tabDivs;

/**
 * Switch to a specific Tab
 * @param {string} name The name of the tab
 */
let switchTab = name =>
{
  // Set all tabs inactive
  [...optionDivs].concat([...tabDivs]).forEach(div =>
  {
    div.classList.remove("active");
  });

  // activate the given tab
  document.querySelector(`#${name}Option`).classList.add("active");
  document.querySelector(`#${name}Tab`).classList.add("active");
};

/**
 * Setup the Tab handling in general
 */
let handleTabs = () =>
{
  // Store away tab divs
  optionDivs = document.querySelectorAll(".option");
  tabDivs = document.querySelectorAll(".tab");

  // Add a click listener to each tab option
  [...optionDivs].forEach(div =>
  {
    div.addEventListener("click", ev =>
    {
      let el = ev.srcElement;
      let option = el.id.replace(/Option/, "");

      switchTab(option);
    });
  });
};

/**
 * Handles URL and filter input
 */
let handleInput = () =>
{
  // URL input
  document.querySelector("#url").addEventListener("keyup", ev =>
  {
    if (ev.keyCode === 13)
    {
      let value = ev.srcElement.value;

      if (/^https?:\/\/.../.test(value))
      {
        document.querySelector("#networkTab").innerHTML = "";
        document.querySelector("#content").src = value;
      }
    }
  });

  // Filter input
  let filterInput = document.querySelector("#filter");
  filterInput.addEventListener("keyup", ev =>
  {
    if (ev.keyCode === 13)
    {
      let value = ev.srcElement.value;

      // CleanUp first
      [...document.querySelectorAll("summary.blocked")].forEach(el =>
      {
        el.classList.remove("blocked");
      });

      if (/^https?:\/\/.../.test(value))
      {
        // Mark network log occurances
        [...document.querySelectorAll("summary")].forEach(el =>
        {
          if (new RegExp(value.replace(/\//g, "\\/").replace(/\./g, "\\.").replace(/\*/, ".*")).test(el.title))

            el.classList.add("blocked");
        });
        // Mark all corresponding images and iFrames
        // [ ...document.querySelector( '#content' )
        //      .contentWindow.document.querySelectorAll( `img` ) ].concat(
        //   [ ...document.querySelector( '#content' )
        //      .contentWindow.document.querySelectorAll( `iframe` ) ]
        // ).filter( ( el ) => {
        //   return ~el.src.indexOf( value );
        // } ).forEach( ( el ) => {
        //   // MarkedElements.push( el );
        //   el.style.border = '1px dashed green';
        //   el.style.outline = '1px dashed red';
        // } );
      }
      filterInputChanged();
    }
  });
};

/**
 * page load handling
 */
let pageLoad = () =>
{
  // Initialize Tabs
  handleTabs();
  handleInput();

  chrome.runtime.sendMessage({
    action: "url"
  });
};

function isSafeRespHeader(header)
{
  let headerName = header.name.toLowerCase();
  return headerName !== "content-security-policy" &&
    headerName !== "referrer-policy" &&
    headerName !== "x-frame-options" &&
    headerName !== "x-permitted-cross-domain-policies" &&
    headerName !== "x-xss-protection";
}

function filterInputChanged()
{
  let filterInput = document.querySelector("#filter").value;
  chrome.runtime.sendMessage({
    action: "filterInputChanged",
    filterInput
  });
}

let requestsDiv;

function urlReceived(url)
{
  requestsDiv = document.querySelector("#networkTab");
  document.querySelector("#content").src = url;
  document.querySelector("#url").value = url;
}

function beforeReqDetailsReceived(details, blocked)
{
  requestsDiv.innerHTML +=
    `<details id="requ_${details.requestId}">
          <summary${blocked ? " class=\"blocked\"" : ""} title="${details.url}">
          ${details.url}</summary>
          &#160;<br/>
          <b>Info</b><br/>
          requestID: ${details.requestId}<br/>
          method: ${details.method}<br/>
          Type: ${details.type}<br/>
            &#160;${blocked ? "Blocked by rule<br/>&#160;" : `<br/>
            <b>Request</b><br/>
            Headers:
            <ul class="requHeader"></ul>
            &#160;<br/>
            <b>Response</b><br/>
            <span class="statusLine"></span><br/>
            Headers:
            <ul class="respHeader"></ul>`}
        </detail>`;
}

function beforeSendHeadersReceived(details)
{
  requestsDiv.querySelector(`#requ_${details.requestId} > .requHeader`)
    .innerHTML = details.requestHeaders.map(entry =>
    {
      return `<li>${entry.name}: ${entry.value}</li>`;
    }).join("");
}


function headersReceived(details)
{
  requestsDiv.querySelector(`#requ_${details.requestId} > .statusLine`)
    .innerHTML = details.statusLine;
  requestsDiv.querySelector(`#requ_${details.requestId} > .respHeader`)
    .innerHTML = details.responseHeaders.map(entry =>
    {
      return `<li${isSafeRespHeader(entry) ? "" : " class=\"gagged\""}>
${entry.name}: ${entry.value}</li>`;
    }).join("");
}

chrome.runtime.onMessage.addListener(
  (request, sender, sendResponse) =>
  {
    switch (request.action)
    {
      case "url":
        urlReceived(request.url);
        sendResponse({});
        break;

      case "beforeRequest":
        beforeReqDetailsReceived(request.details, request.blocked);
        sendResponse({});
        break;

      case "beforeSendHeaders":
        beforeSendHeadersReceived(request.details);
        sendResponse({});
        break;

      case "onHeadersReceived":
        headersReceived(request.details);
        sendResponse({});
        break;

      default:
        console.log("Filterlyzer: Got an unexpected message:", request.action);
    }
  }
);

// Initialize tabs
window.addEventListener("DOMContentLoaded", pageLoad);
