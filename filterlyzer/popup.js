/* eslint-env browser, webextensions */

"use strict";

function analyzeThis()
{
  chrome.tabs.query(
    {
      active: true, currentWindow: true
    },
    tabs =>
    {
      chrome.runtime.sendMessage("", {
        action: "analyzeThis",
        tabId: tabs[0].id
      });
    });
}

let b = window.document.querySelector("#analyze_this");
b.onclick = analyzeThis;
