/* eslint-env webextensions, browser */
/* eslint no-console: 0 */

"use strict";

/**
 * Holds the collected data per tab
 */
let tabData = {};
/**
 * Holds an oparating mode
 */
let mode = "MARK";
/**
 *
 */
let cvProviders = {
  AdDefend:
  {
    color: "#005500",
    headerDetector(header)
    {
      return header.name.toLowerCase() === "x-powered-by" &&
        header.value.toLowerCase() === "addefend gmbh";
    }
  },
  Tisoomi:
  {
    color: "#3333cc",
    headerDetector(header)
    {
      return header.name.toLowerCase() === "x-powered-by" &&
        header.value.toLowerCase() === "tsadserver";
    },
    urlDetector(url)
    {
      // console.log(`checking url detector for ${url}`);
      return url.toLowerCase().indexOf("tisoomi-services.com") > 0;
    },
    requestDetector(header)
    {
      // console.log( 'checking request detector for', header );

      // Obviously it would be much nicer to search for a set-cookie response
      // header, but I can't find the place, where this cookie is set...

      // This works pretty fine for the first call, as ads are usually
      // loaded after content images, but
      //   1) that's not reliable
      //   2) obviously cookies stay for the second call
      return header.name.toLowerCase() === "cookie" &&
        header.value.toLowerCase().indexOf("_tstfc=") > -1;
    }
  },
  InstartLogic:
  {
    color: "#5555ee",
    headerDetector(header)
    {
      return ~header.name.toLowerCase().indexOf("x-instart-");
    }
  },
  AntiAdblock:
  {
    color: "#04b484",
    headerDetector(header)
    {
      return (
        header.name.toLowerCase() === "x-nginx-proxy" &&
          (" " + header.value).toLowerCase().indexOf(" aab-") >= 0
      ) || (
        header.name.toLowerCase() === "x-nginx-proxy" &&
          header.value.toLowerCase().indexOf("antiadblock-") >= 0
      ) || (
        header.name.toLowerCase() === "x-server" &&
          header.value.toLowerCase().indexOf("aab") === 0
      );
    }
  }
};

/**
 * Sets the badge for a specific tab
 * @param {integer} tabId The tab id.
 * @param {string} text The badge text.
 * @param {string} color The color of the badge.
 */
function setBadge(tabId, text, color)
{
  chrome.browserAction.setBadgeBackgroundColor({
    color,
    tabId
  });
  chrome.browserAction.setBadgeText({
    text,
    tabId
  });
}

/**
 * Mark and block resources based on headers
 * @param {integer} tabId The tab id to act on.
 * @param {object} details
 * @return {webRequest.BlockingResponse} Whether to block or not.
 */
function markOrBlock(tabId, details)
{
  if (details.type !== "stylesheet")
  {
    if (mode === "BLOCK" && details.type !== "main_frame")
    {
      return {cancel: true};
    }
    else if (mode === "MARK")
    {
      chrome.tabs.sendMessage(tabId, {
        action: "markElement",
        url: details.url,
        type: details.type
      });
    }
  }
  return void 0;
}

/**
 * Creates data skeleton for tabs
 * @param {integer} tabId The tab id.
 */
function createTabDataSkeleton(tabId)
{
  tabData[tabId] = tabData[tabId] || {
    objects: []
  };
}

/**
 * Log away a CV provider finding
 * @param {integer} tabId The tab id to log for.
 * @param {object} cvProvider CV provide to log.
 */
function logCV(tabId, cvProvider)
{
  chrome.tabs.get(tabId, tabInfo =>
  {
    let storageItem = {};

    storageItem[tabInfo.url.replace(/^[^:]+:\/\/([^/]+)\/.*$/, "$1")] = {
      cvProvider,
      dateTime: Date.now()
    };
    chrome.storage.local.set(storageItem);
  });
}

/**
 * Handles the identification of a specific CV provider
 * @param {integer} tabId The tab id to identify for.
 * @param {object} cvProvider CV provide to log.
 * @param {object} details
 */
function handleIdentification(tabId, cvProvider, details)
{
  let thisData = tabData[tabId];

  thisData.objects.push(details);

  if (!thisData.provider)
  {
    thisData.provider = cvProvider;

    logCV(tabId, cvProvider);

    // Set Badge for tab
    setBadge(tabId, cvProvider, cvProviders[cvProvider].color);
  }
}

/**
 * Adds a listener to the onHeadersReceived event
 * It runs over the header detector functions and modifies the collected
 * data in tabData
 */
chrome.webRequest.onHeadersReceived.addListener(
  details =>
  {
    let tabId = details.tabId;

    // Create data for the current tab, if not yet existing
    createTabDataSkeleton(tabId);

    // Iterate over cvProviders
    for (let providerId in cvProviders)
    {
      if (details.responseHeaders.filter(
        cvProviders[providerId].headerDetector
      ).length)
      {
        handleIdentification(tabId, providerId, details);

        // Do Marking or Blocking of elements
        return markOrBlock(tabId, details);
      }
    }
    return void 0;
  },
  {urls: ["<all_urls>"]},
  ["responseHeaders", "blocking"]
);

/**
 * Adds a listener to the onBeforeRequest event
 * It checks the url modifies the collected data in tabData
 */
chrome.webRequest.onBeforeRequest.addListener(
  details =>
  {
    let tabId = details.tabId;

    // Create data for the current tab, if not yet existing
    createTabDataSkeleton(tabId);

    // Iterate over cvProviders
    for (let providerId in cvProviders)
    {
      let urlDetector = cvProviders[providerId].urlDetector;

      if (urlDetector && urlDetector(details.url))
      {
        handleIdentification(tabId, providerId, details);

        // Do Marking or Blocking of elements
        if (mode === "BLOCK")
          return false;
      }
    }
  },
  {urls: ["<all_urls>"]},
  ["blocking"]
);

/**
 * Adds a listener to the onBeforeSendHeaders event
 * It runs over the url and request detector functions and modifies the
 * collected data in tabData
 */
chrome.webRequest.onBeforeSendHeaders.addListener(
  details =>
  {
    let tabId = details.tabId;

    // Create data for the current tab, if not yet existing
    createTabDataSkeleton(tabId);

    // Iterate over cvProviders
    for (let providerId in cvProviders)
    {
      let thisProvider = cvProviders[providerId];

      if (thisProvider.urlDetector &&
          thisProvider.urlDetector(details.url) ||
          thisProvider.requestDetector &&
          details.requestHeaders.filter(thisProvider.requestDetector).length)
      {
        handleIdentification(tabId, providerId, details);

        // Do Marking or Blocking of elements
        return markOrBlock(tabId, details);
      }
    }
    return void 0;
  },
  {urls: ["<all_urls>"]},
  ["requestHeaders", "blocking"/* , "extraHeaders" */]
);

/**
 * Tab Handling: Close Tab
 * Removes collected data for the tab
 */
chrome.tabs.onRemoved.addListener(
  tabId /* , removeInfo */ =>
  {
    delete tabData[tabId];

    // console.log( 'Discard Tab', tabId );
  }
);

/**
 * Tab Handling: Update Tab
 * Removes collected data for the tab
 */
chrome.tabs.onUpdated.addListener(
  (tabId, changeInfo /* , tab */) =>
  {
    if (changeInfo && changeInfo.status === "loading")
    {
      tabData[tabId] = {
        objects: []
      };
      // console.log( 'Update Tab', tabId, ':', changeInfo );
    }
  }
);

// Message Receivers
chrome.runtime.onMessage.addListener(
  (request, sender, sendResponse) =>
  {
    switch (request.action)
    {
      case "getTabData":
        // console.log( 'Tab Id:', request.tabId );
        sendResponse(tabData[request.tabId]);
        break;

      case "getMode":
        sendResponse(mode);
        break;

      case "setMode":
        mode = request.mode;
        sendResponse({});
        break;

      case "debug":
        console.log("Debug msg:", request.msg);
        sendResponse({});
        break;

      default:
        console.log("Got an unexpected message:", request.action);
        sendResponse({});
    }
  });
