/* eslint-env browser, webextensions */

"use strict";

/**
 * Whether we want debugging on or off
 */
let DEBUG = false;

/**
 * Populates debug messages - mainly by transferring them to the background
 * page to have them logged there
 *
 * @param {string} msg The message to output.
 */
function debug(msg)
{
  if (DEBUG)
  {
    chrome.runtime.sendMessage("", {
      action: "debug",
      msg
    });
  }
}

/**
 * Visualizes the data we have for a given tab
 * @param {object} tabData The tab data.
 */
function visualizeTabData(tabData)
{
  let verdictDiv = document.querySelectorAll("#verdict")[0];
  let detailsDiv = document.querySelectorAll("#details")[0];
  let providerImg = "none";
  let verdictHTML = "<b style=\"color: #ccffcc\">no known</b>";
  let detailsHTML = "";

  // Overview (verdict) information
  if (tabData && tabData.provider)
  {
    verdictHTML = `<b style="color: #ffcccc">${tabData.provider}</b>`;
    providerImg = tabData.provider.toLowerCase();
  }
  verdictDiv.innerHTML = `This site uses ${verdictHTML} circumvention`;
  verdictDiv.style.backgroundImage = `url(img/${providerImg}.png)`;

  // Detailed information about loaded ressources, that are associated to a
  // CV provider
  if (tabData && tabData.objects)
  {
    for (let el in tabData.objects)
    {
      let obj = tabData.objects[el];
      detailsHTML += `<div title="${obj.url} (${obj.type})">${obj.url}</div>`;
    }
  }
  detailsDiv.innerHTML = detailsHTML;
}

/**
 * Handles the mode checkboxes on the bottom of the popup
 * @param {string} mode Either "BLOCK" or "MARK".
 */
function handleCheckBoxes(mode)
{
  let modeBlock = document.querySelectorAll("#modeBlock")[0];
  let modeMark = document.querySelectorAll("#modeMark")[0];
  switch (mode)
  {
    case "BLOCK":
      modeBlock.checked = true;
      break;
    case "MARK":
      modeMark.checked = true;
      break;
  }

  // Send block mode state to background page
  modeBlock.onchange = () =>
  {
    modeMark.checked = false;
    chrome.runtime.sendMessage("", {
      action: "setMode",
      mode: modeBlock.checked ? "BLOCK" : "NONE"
    });
  };

  // Send mark mode state to background page
  modeMark.onchange = () =>
  {
    modeBlock.checked = false;
    chrome.runtime.sendMessage("", {
      action: "setMode",
      mode: modeMark.checked ? "MARK" : "NONE"
    });
  };
}

/**
 * page load script
 * Requests data about the current tab, visualizes it and finally sets the
 * mode checkboxes correctly
 */
function onLoad()
{
  debug("popup opened");

  // Get current active tab
  chrome.tabs.query(
    {
      active: true, currentWindow: true
    },
    tabs =>
    {
      // Get and visualize collected data for this tab
      chrome.runtime.sendMessage(
        "", {
          action: "getTabData",
          tabId: tabs[0].id
        },
        {},
        response =>
        {
          visualizeTabData(response);
        }
      );


      // fetch current mode an set checkboxes accordingly
      chrome.runtime.sendMessage(
        "", {
          action: "getMode"
        },
        {},
        response =>
        {
          handleCheckBoxes(response);
        }
      );
    }
  );
}

window.addEventListener("DOMContentLoaded", onLoad);

