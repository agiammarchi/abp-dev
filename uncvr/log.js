/* eslint-env browser, webextensions */

"use strict";

/**
 * shows the gathered log data in a new tab (or window)
 */
let showLogData = () =>
{
  chrome.storage.local.get(null, logData =>
  {
    document.querySelector("#logData").innerHTML =
      Object.keys(logData).map(key =>
      {
        let value = logData[key];
        return `<tr><td>${key}</td>
<td>${value.cvProvider}</td>
<td>${new Date(value.dateTime)}</td></tr>`;
      }).join("");
  });
};

window.addEventListener("DOMContentLoaded", showLogData);
